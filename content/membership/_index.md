---
title: "Hackerspace membership"
linktitle: "Hackerspace membership"
---

Being a space member has benefits but of course also some responsibilities.  
-- With great power comes great responsibility.

# Benefits

- 24/7 access
  - Every member always has access to the space.
- Member storage
  - Every member can get a member shelf AKA a drawer, to store personal stuff.
  - A personal Nextcloud account with 1GB storage space by default
- nextcloud access
  - there is a nextcloud instance managed and hosted by the hackerspace, an account on it is included in the membership

# Responsibilities

- There are no passengers on this ship, only crew members.
- We are explorers, no pirates.
- Upon leaving the ship, clean your spot, and the spot next to you.
- Drinks are in the fridge, they are cool, cheap and help us stay afloat.
- Respect fellow members, their privacy and hardware.
- Communication generally works better than conflict.
- The guy/girl mopping the deck deserves more respect than the non-existing captain.

Tip:

- Sharing is good, share your knowledge, creativity, hardware and code.
- Respect is not enforced, it is earned.
- Parrots have no original thoughts. Think, don't be a parrot.
- Truth is usually somewhere in the middle. Most issues aren't binary anyway. Requantize.
- Last but foremost: Be excellent to eachother.

-- From https://wiki.hsbxl.be/Patterns

# Steps on becoming a member

- Come over for [TechTuesday](/events/techtuesday) a few times. Get to know some members, talk about your interests.
- Fill out the membership form.
- Find 2 sponsors to sign your membership form.
- Once processed, you will receive a welcome mail with paying instructions.
- Once we receive your payment, will get access to the .

# Paying instructions?

Yes. As much as we would like otherwise,  we need to pay bills to keep the ship afloat (rent, electricity, heating, etc.).

That's why we ask a membership fee of minimum **€30 a month**. 
In reality we need about **€40 a month** to break even, so feel free to give more or to donate to the space when you visit.

## What are our fixed costs?

- Rent: €903/month
- Heating/electricity/water: +- €429/month
- Insurance: ?/year
- Legal costs (ex. statute changes): +- €100/year

Further we have nice tools and spend money at keeping it that way.

# Just stating the obvious

- a hackerspace is not a meetup place for criminals (don't ask us to hack the account of your ex).
- local law is also applicable here. Don't break it.
