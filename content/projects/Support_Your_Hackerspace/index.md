---
title: "Support Your Hackerspace"
linktitle: "support_your_hackerspace"
state: running
maintainer: "askarel"
---

We had a new delivery: home deliveries are resuming...

![This crate to your home](./clubmate.jpg)

The COVID-19 pandemic is affecting all of us, and one of the consequence is that
you cannot go easily (or at all) to your hackerspace, so you don't get your fix
of club-mate, and the hackerspace does not get all the funds it needs to exist.

Behold! There is a solution !

We can deliver a crate to your door! Yes, your own personal supply of club-mate
in your confinement space !

All you need to do is send an e-mail to clubmate@hsbxl.be, with the address of
where you want it delivered, and the amount of crates desired.

The crates are sold at hackerspace price (40€ per crate). Payment must be done on
HSBXL bank account: BE69 9794 2977 5578

For logistics reasons, there are some limitations:

- you can only buy by crates,
- Crates will be delivered in a radius of 10km around Brussels.
- Deliveries will happen on monday evenings only
- Order and payment must arrive latest on friday evening for a next monday delivery.
