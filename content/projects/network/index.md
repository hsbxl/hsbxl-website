---
title: "Network/squirrelco"
linktitle: "The internet"
state: running
maintainer: "altf4"
---

# We have internet \o/

And we even have a webpage for the internet. It turns out that we went a little bit overkill on that one. So the website is here : [Squirrelco](https://squirrelco.net)

The explanation as to why it is called SquirrelCo is also explained on the website ;-)
