---
title: "OctoPi for the space"
linktitle: "OctoPi @HSBXL"
date: 2021-09-01
state: running
maintainer: "jurgen"
---

# Context and background

    OctoPi is a simple interface for managing (an) 3D-printer(s).
    Currently a base install is done on a RasPi with a PiCam.
    The device can be reached internally at http://octopi.in.hsbxl

    Endgoal:
    - The idea is to be able to send .stl files to the printer
    - The idea is that users can log in with their LDAP account
    - Do we want the webcam to be publicly accessible? (currently not)

# To Do

- setting up LDAP integration
  - starting point: https://github.com/gillg/OctoPrint-LDAP
  - critical review of code as it is not part of the plugin library
- there is a plugin that works with slic3r on the pi. This needs to be populated with a few good configurations for the Lulzbot or Ender
  - https://plugins.octoprint.org/plugins/slic3r/
- creating an enclosure to click the octopi on the lulzbot in a way the cam has a good angle on the print
  - currently WIP by Jurgen in FreeCAD.
- fixing the Velleman 3D printer
  - this device is looking for a friendly pair of hands to fix it.
