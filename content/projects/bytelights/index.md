---
title: "ByteLights"
linktitle: "ByteLights"
state: running
maintainer: "Jegeva"
---

The content has been moved to [Nextcloud](https://cloud.hsbxl.be/index.php/s/xqzsoGCRWmHLA4r?path=%2Fbytelights) in order to make the website more lightweight.
