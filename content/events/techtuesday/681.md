---
techtuenr: "681"
startdate: 2023-05-16
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue681
series: TechTuesday
title: TechTuesday 681
linktitle: "TechTue 681"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
