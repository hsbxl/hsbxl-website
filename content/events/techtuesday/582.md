---
techtuenr: "582"
startdate: 2021-06-22
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue582
series: TechTuesday
title: TechTuesday 582
linktitle: "TechTue 582 "
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
