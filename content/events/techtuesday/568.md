---
eventid: techtue568
startdate: 2020-05-12
starttime: "19:00"
linktitle: "TechTue 568"
title: "TechTuesday 568"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
