---
techtuenr: "666"
startdate: 2023-01-31
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue666
series: TechTuesday
title: TechTuesday 666
linktitle: "TechTue 666"
location: HSBXL
image: techtuesday.png
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
