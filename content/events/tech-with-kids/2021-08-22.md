---
startdate: 2021-08-22
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids"
title: "Tech with kids (NL-FR-EN-...)"
price: "Free"
image: "tech_with_kids.jpg"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

today we all work on our own projects. No theme.
