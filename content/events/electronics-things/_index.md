---
linktitle: "Electronics - some basic stuff"
title: "Electronics - some basic stuff"
---

Lots of things to be said and done about electronics. Trying to figure them out in ad hoc events...

## Upcoming events

{{< events when="upcoming" series="electronics" >}}

## Past events

{{< events when="past" series="electronics" >}}
