---
startdate: 2019-01-30
starttime: "19:00"
enddate: 2019-01-30
linktitle: "Stompboxin’, DIY fuzzboxes for bass and guitar"
title: "Stompboxin’, DIY fuzzboxes for bass and guitar"
location: "HSBXL"
eventtype: "Workshop"
price: "30€ for the HSBXL-Fuzz stompbox kit"
series: "byteweek2019"
image: "stompbox-hsbxl.png"
---

To celebrate the awesomeness of HSBXL, we've designed a limited edition HSBXL-Fuzz pedal for guitarists, bass-players and other musicians that want to properly mess up their sound-fidelity.

The design is made to be 'mod-friendly' and multiple options for assembly will be proposed. To fund the PCB printing costs, components and aluminium die-cast enclosure, a participatory fee of +-€30,- is proposed.

The schematics and relevant build-documents will be handed out during the workshop.

- Max number of participants: 15
- If you can, bring your own soldering iron.
- Duration: Experienced builders 1h, Newbies 4hs
