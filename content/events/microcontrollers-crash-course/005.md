---
mccrashcoursenr: "744"
startdate: "2024-08-17"
starttime: "14:30"
endtime: "18:30"
price: "free"
eventtype: "Workshop"
series: "Arduino and microcontrollers"
title: "Arduino and microcontrollers 005"
linktitle: "Arduino and microcontrollers 005"
location: HSBXL
image: "chip-indigo.png"
---

## Language

This workshop will be given in French and English.

## Arduino and microcontroller : Crash Course N°005

**Program :**

- **implement gameplay** on the Simon game

- **introduce the seven-segment display** that we'll use for showing the score :

  - what's inside the box ?

  - how to display a specific digit ?

  - how to convert a serial signal into a parallel signal ?
