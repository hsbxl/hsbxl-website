---
mccrashcoursenr: "744"
startdate: "2024-08-24"
starttime: "14:30"
endtime: "18:30"
price: "free"
eventtype: "Workshop"
series: "Arduino and microcontrollers"
title: "Arduino and microcontrollers 006"
linktitle: "Arduino and microcontrollers 006"
location: HSBXL
image: "chip-indigo.png"
---

## Language

This workshop will be given in French and English.

## Arduino and microcontroller : Crash Course N°006

**Program :** tbd after session 5
