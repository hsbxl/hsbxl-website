---
linktitle: "Merda Elettronica"
title: "Merda Elettronica"
---

amorfous collective of electronic's loud  
freaks & DIY punk trouble makers,  
spreading circuit bending, noises,  
electronic abuses and hacking attitude  
around the globe.

https://merda-elettronica.hotglue.me

## Upcoming Merda Elettronica events

{{< events when="upcoming" series="merda_elettronica" >}}

## Past Merda Elettronica events

{{< events when="past" series="merda_elettronica" >}}
