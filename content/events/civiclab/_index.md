---
linktitle: "Civiclab"
title: "Civiclab Events"
location: "HSBXL"
eventtype: "workshop"
price: "Free"
aliases: [/civiclab/]
series: "civclabs"
start: "true"
---

Civic Lab Brussels is an civic-action-driven group for those interessed in creating grassroots solutions

## Upcoming Open Coding Day Events

{{< events when="upcoming" series="civiclab" >}}

## Past Open Coding Day Events

{{< events when="past" series="civiclab" >}}
