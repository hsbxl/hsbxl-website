---
startdate: 2019-11-30
starttime: "21:00"
endtime: "04:00"
linktitle: "Hash#hasH:Algorave"
title: "Hash#hasH"
location: "HSBXL"
eventtype: "Live-music/coding visuals"
price: "FREE ENTRANCE"
---

# hash#hash Algorave

Hash#hash is a new series of live-coding nights. We're taking off
with an algorave at the Hackerspace near Gare du Midi, Brussels. Live-coding sometimes referred to as conversational programming, is the act of writing code live, as a performance. An
algorave (from an algorithm and rave) is an event where people  
dance to music generated from algorithms, often using live coding.

# Line up

- dagobert sondervan
- exoterrism
- ACOUSTIC DEVICES
- Leaky Abstraction
- Julien Bloit
- Bunex
- hogobogobogo && gibby-dj

# Practical information

Dress warm!
How to get to Hackerspace Brussels: 15 minutes from Brussels-Midi by foot
Metro: Brussels-Midi – Bus: 78 Deux Gares – Tram 81: Cureghem

# ln -s

- facebook: https://www.facebook.com/events/1525168837621982/
