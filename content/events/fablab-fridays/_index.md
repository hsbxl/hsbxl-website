---
linktitle: "fablab Fridays"
title: "fablab Fridays"
location: "HSBXL"
eventtype: ""
price: "Free"
aliases: [/fablabFriday/]
series: "FablabFriday"
start: "true"
---

Every first Friday of the month we are having a day dedicated to building stuff

## Upcoming FablabFridays

{{< events when="upcoming" series="FablabFridays" >}}

## Past FablabFridays

{{< events when="past" series="FablabFridays" >}}
