---
startdate: 2021-08-17
starttime: "18:00"
endtime: "19:00"
linktitle: "Working with GIT and version control"
title: "Working with GIT and version control"
price: "Free"
series: "website"
eventtype: "members"
location: "HSBXL"
---

GIT is typically used by developers to manage changes in programming code. Yet, there are much more ways in which GIT can be used outside of the programming world.

Wouter will give an introduction into the principles of GIT. This can be a goot preparation for the following workshop on adding content to the website (surprise: we're also using GIT there)

**Requirements:** Have [GIT](https://git-scm.com/) installed on your computer.
Thank you Wouter
this edit prooves I was able to do stuff afterwards
