---
linktitle: "Working with the website"
title: "Working with the website"
---

HSBXL has a website (yes, this one). Let's see how it works.

## Upcoming events

{{< events when="upcoming" series="website" >}}

## Past events

{{< events when="past" series="website" >}}
