---
linktitle: Post-FOSDEM Open Coding Day
series: Open Coding Day
title: "Post-FOSDEM Open Coding Day: Co-work and Connect"
location: HSBXL
eventtype: co-working event
startdate: "2024-02-05"
starttime: "11:00"
endtime: "18:00"
image: open-coding-day.png
---

## Post-FOSDEM Open Coding Day: Co-work and Connect

Fresh off the heels of FOSDEM, we're opening our doors at HSBXL for a special co-working day for those still in the city and looking to connect, co-work, and wind down after the conference. Whether you're looking to get some remote work done, share your FOSDEM experiences, or just want to hang out in a community environment, this day is for you.

During this **7-hour co-working event**, not only will you have the chance to work on your projects, but it's also a great opportunity to network with fellow FOSDEM attendees, discuss your takeaways from the conference, and collaborate on new ideas sparked over the weekend.

**Please Note**: While HSBXL is equipped with multiple rooms catering to different needs, this post-FOSDEM co-working day will take place in the main room. We kindly ask that online meetings be kept to a minimum to avoid noise disturbances and ensure a productive atmosphere for everyone.

### What the day will look like

1. Welcome and Settling In: Find a spot in the main room, set up your workstation, and kickstart your day with a beverage of your choice.
2. Co-working Session: Immerse yourself in your work, reach out for feedback, collaborate with peers, or simply enjoy the communal vibe.
3. FOSDEM Reflections: We encourage sharing your FOSDEM stories, learnings, and any exciting opportunities or projects you discovered.

### Getting to HSBXL

HSBXL is situated in the heart of Brussels. Detailed directions to our hackerspace can be found [here](https://hsbxl.be/enter/). Should you face any difficulties getting in, contact us at +32 28804004. The phone is located inside the space, but you can also connect with us via our [Matrix chatroom](https://matrix.to/#/#hsbxl:matrix.org).

### What to Bring

You can bring your laptop. WiFi is available on-site, having a backup internet solution is always beneficial.

We invite you to join us for this special **7-hour co-working day** at HSBXL. It's a wonderful opportunity to progress on your projects, forge new connections, and share in the spirit of community collaboration. This event is open to everyone - whether you're a seasoned developer, a beginner, or simply someone with an interest in tech and coding. Come along and let's continue the energy and inspiration from FOSDEM together!
