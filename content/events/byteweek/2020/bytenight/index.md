---
startdate: 2020-02-01
starttime: "18:00"
linktitle: "Bytenight"
title: "Bytenight 2020"
location: "HSBXL"
eventtype: "Thé dansant"
price: ""
image: "bytenight.png"
series: "byteweek2020"
---

As an after-party to the big FOSDEM Free Software conference in Brussels,  
the oldest Hackerspace in town organises its annual free party.

# Version 11 (0xb)

(space to be filled in)

# Music

Weird music for weird people.

## Lineup

- Something Nasty
- Mike Rushmore
- Hogobogobogo
- [Jonny and the Bomb](https://soundcloud.com/4a-6f-6e-6e-79-20-2b-20-5)
- Joostvb
- awd
- [Planete Concrete](http://planeteconcrete.tumblr.com/)

# Unsupported, deprecated versions

- [Bytenight v2019](https://hsbxl.be/events/byteweek/2019/bytenight/)
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](<https://wiki.hsbxl.be/Bytenight_(2016)>)
- [Bytenight v2015](<https://wiki.hsbxl.be/Bytenight_(2015)>)
- [Bytenight v2014](<https://wiki.hsbxl.be/Bytenight_(2014)>)
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_Bye_Bye_Garage)
- [Bytenight v2012](<https://wiki.hsbxl.be/ByteNight_(2012)>)
- [Bytenight v2011](<https://wiki.hsbxl.be/ByteNight_(2011)>)
- [Bytenight v2010](<https://wiki.hsbxl.be/ByteNight_(2010)>)

# Organizing

Notes can be found on https://etherpad.openstack.org/p/bytenight2020
