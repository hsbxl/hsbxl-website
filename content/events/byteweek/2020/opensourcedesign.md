---
startdate: 2020-01-31
starttime: "12:00"
enddate: 2020-01-31
endtime: "17:00"
allday: false
linktitle: "Open Source Design Workshop"
title: "Open Source Design Workshop"
location: "HSBXL"
eventtype: "Workshop/hackathon"
price: "Free"
series: "byteweek2020"
image: "opensourcedesignworkshop.jpg"
---

# Cheap, easy and illuminating – how to plan, run and analyse usability studies

A free, hands-on session on how to plan, run, capture and analyse usability studies with software users. We have designed this session specifically for free and open source projects, and it is suitable for complete newbies: no experience on design research necessary.

**We will cover:**

- Planning usability studies: research questions, participant recruitment, protocol, technical set-up and research script
- Running usability studies: how to moderate and document a usability testing session
- Analysis: how to make sense of a usability study with affinity diagrams

You will create your own research plan using a software project of your choice as a case study. We will run a usability testing session in the room that you will need to document as it happens. Finally, we will use that documentation to analyse the outcome of the usability testing session by building an affinity diagram.

What you need to bring: you must arrive with a chosen software project as your case study and bring a pen.
