---
startdate: 2020-01-31
starttime: "18:30"
enddate: 2020-01-31
endtime: "23:00"
allday: false
linktitle: "Pre FOSDEM Kubernetes Warmup"
title: "Pre FOSDEM Kubernetes Warmup"
location: "HSBXL"
eventtype: "Workshop/hackathon"
price: "Free"
series: "byteweek2020"
image: "kubernetes-belgium.png"
---

More details on the program can be found on the Meetup page.

Meetup: https://www.meetup.com/kubernetes-belgium/events/267744647/
