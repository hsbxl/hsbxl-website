---
startdate: 2023-01-30
enddate: 2023-02-04
allday: true
linktitle: "Byteweek 2023"
title: "Byteweek 2023"
location: "HSBXL"
eventtype: "5 days of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2023"
start: "true"
---

The week before Fosdem conference, HSBXL compiles a week of hacking.  
bin(111) days of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

# Monday January 30th

{{< events series="byteweek2023" when="2023-01-30" showtime="true" showallday="true" >}}

# Tuesday January 31st

{{< events series="byteweek2023" when="2023-01-31" showtime="true" showallday="true" >}}

# Wednesday February 1st

{{< events series="byteweek2023" when="2023-02-01" showtime="true" showallday="true" >}}

# Thursday February 2nd

{{< events series="byteweek2023" when="2023-02-02" showtime="true" showallday="true" >}}

# Friday February 3rd

{{< events series="byteweek2023" when="2023-02-03" showtime="true" showallday="true" >}}

# Saturday February 4th

{{< events series="byteweek2023" when="2023-02-04" showtime="true" showallday="true" >}}

# Call for participation

If you want to do a talk, a workshop or have a hackaton,  
send a mail to **contact@hsbxl.be** with your proposal.
