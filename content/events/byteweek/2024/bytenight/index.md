---
startdate: 2024-02-03
starttime: "20:00"
linktitle: "Bytenight"
title: "Bytenight 2024"
location: "HSBXL"
eventtype: "Thé dansant"
price: "pay for your drinks please"
image: "bytenight2024.webp"
series: "byteweek2024"
aliases: [/bytenight/]
---

# 🎉 ByteNight 2024 is coming up! 🎉

Hackerspace Brussels (HSBXL) is again organizing it's unofficial FOSDEM afterparty on February 3 2024. And this time it will be even bigger and better than last year.

We invite all people from the hackerspaces and FLOS communities (FOSDEM, OFFDEM, ...) to come join us for a night of socializing, dancing or just having a club mate or beer. Here you will find all information related to the event. This page might get updated as certain details get confirmed or changed.

# Entrance

Entrance to the event is free of charge for visitors of FOSDEM, OFFDEM and people linked to the Hacking & FLOS Communities at large. The doorperson may ask some arbitrary question to verify this. We don't like to gatekeep, but as this is a private event we need some way of firewalling out people who are not familiar with the community and are just looking for a party to crash.

# Location

ByteNight will take place just like last year inside Hackerspace Brussels (HSBXL). The entrance to the event will be Rue de la Petite île 1, 1070 Anderlecht.

This edition will be the last on our address in Anderlecht as the building will be demolished later this year, so come say farewell with us. (But please don't start the demolishing just yet) Exact instructions how to get to the space can be found [HERE](https://hsbxl.be/enter/)

We are looking into claiming more space in the building compared to last year so maneuverability will be improved.

Also important to note is that the space is on the 3th floor and regrettably the elevator is permanently out of order. In the long term these accessibility issues will be solved by moving to our new location. But for this edition this is still something to consider.

# Photography/Filming

Camerapeople: please note that not everyone at the event will be comfortable with having their picture taken or appear on camera. If you really want to take a picture or a video, try not to include anyone on it or at the very least ask for and respect their consent. And as always, be excellent to each other.

# Music & Light Show Installation

Just like last year we have dedicated a hall to dancing and music and we invited a roster of DJ's both familiar and new to ByteNight. This will be accompanied by a light show installation created by members of HSBXL.

The music will be brought by the following DJ's:

    DJ CedrickHacksThePlanet
    DJ Sheitan
    Joe D'absynth
    Arthychoc

# Catering by Fermenthings

This year, we're excited to announce that our catering will be provided by [Fermenthings](https://web.archive.org/web/https://www.fermenthings.be/), experts in the art of fermentation. Fermenthings specializes in creating sustainable and innovative culinary experiences through the use of fermentation techniques. They offer a range of unique products including beers, wines, ciders, and specialty fermented foods. We're featuring [Pandora's Snack Box](https://web.archive.org/https://archive.is/sBwtz) a unique and delicious offering that captures the essence of Fermenthings' expertise.

# Drinks & Light Snacks

A selection of Club Mate, soft drinks and Belgian beers (alcoholic and non-alcoholic) will be available at the bar.

We will also have some food options available (including vegan) for people who came hungry from the conference or get hungry while partying.

# Event Merch

There will be people operating a screen printing installation during the event so visitors have a chance to have their t-shirts printed with a ByteNight 2024 design for a free contribution. We will have a small stock of shirts available but bring your own t-shirt/hoodie to assure full body compatibility.

# Health & Safety Notice

Following the latest guidelines from the Belgian government, there are no mask mandates or COVID-19 related restrictions for our event. We won't implement any additional rules.

Nevertheless, with COVID-19 and other pathogens still among us, wearing a mask is recommended for everyone's safety. If you need a mask, please ask one of our volunteers; we have a small supply ready. To further ensure safety, we'll maintain good ventilation throughout the venue and make better use of the available space to minimize crowding.

If you're not feeling 100% healthy, please consider staying home. We look forward to more events in the future where we can celebrate together safely.

# Organizing

Notes can be found on https://etherpad.openstack.org/p/bytenight2023

# [EVENT WEBSITE](https://bytenight.brussels)

[CONTACT PAGE](https://hsbxl.be/contact/)
[getting to the location](https://hsbxl.be/enter/)

# Unsupported, deprecated versions

- [Bytenight v2023](https://hsbxl.be/events/byteweek/2023/bytenight/)
- [Bytenight v2020](https://hsbxl.be/events/byteweek/2020/bytenight/)
- [Bytenight v2019](https://hsbxl.be/events/byteweek/2019/bytenight/)
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](<https://wiki.hsbxl.be/Bytenight_(2016)>)
- [Bytenight v2015](<https://wiki.hsbxl.be/Bytenight_(2015)>)
- [Bytenight v2014](<https://wiki.hsbxl.be/Bytenight_(2014)>)
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_Bye_Bye_Garage)
- [Bytenight v2012](<https://wiki.hsbxl.be/ByteNight_(2012)>)
- [Bytenight v2011](<https://wiki.hsbxl.be/ByteNight_(2011)>)
- [Bytenight v2010](<https://wiki.hsbxl.be/ByteNight_(2010)>)

# Organizing

Notes can be found on https://etherpad.openstack.org/p/bytenight2023
)
