---
startdate: 2025-02-02
starttime: "19:00"
enddate: 2025-02-02
endtime: "22:00"
allday: false
linktitle: "Social Web After Hours"
title: "Social Web After Hours"
location: "HSBXL"
eventtype: "Meetup"
price: "Free"
series: "byteweek2025"
---

# Social Web After Hours

The [Social Web track](https://www.fosdem.org/2025/schedule/track/social-web/) coordinators from FOSDEM 2025 are organizing an additional side event to continue discussions and engage with the vibrant social web community. This event is ideal for those who couldn't get enough during the FOSDEM weekend or want to dive deeper into the topics covered.

## Event Details

### Talks

At this event on the closing night of FOSDEM, four well-known Fediverse speakers will share their knowledge and experience:

- **Darius Kazemi:** Presenting the Fediverse Observatory.
- **Christine Lemmer-Webber and Jessica Tallon:** Discussing their latest work as ActivityPub co-editors.
- **Julian Tam:** Talking about NodeBB and ActivityPub for forums.
- **Matthias Pfefferle:** Presenting the ActivityPub plugin for WordPress.

A Q&A session will follow the talks.

### Venue

The venue offers space for up to 50 participants, with a main room featuring a large TV for talks and presentations. Additional rooms will be available for informal discussions and networking over snacks and drinks.

## Join Us

Whether you’re a speaker from the [FOSDEM Social Web track](https://www.fosdem.org/2025/schedule/track/social-web/) or a community member interested in learning and sharing ideas, this meetup is open to all. Attendance is free!

### Contact

For more details, reach out to Evan Prodromou via:

- **Email:** [evan@socialwebfoundation.org](mailto:evan@socialwebfoundation.org)
- **ActivityPub:** @evanprodromou@socialwebfoundation.org

We look forward to seeing you there!
