---
startdate: 2019-01-29
starttime: "19:00"
enddate: 2019-01-29
linktitle: "Introduction to LDAP"
title: "Introduction to LDAP"
location: "HSBXL"
eventtype: "Talk/Workshop"
price: "Free"
series: "byteweek2019"
---

Introduction to LDAP

A small introduction to LDAP and how it can make your life easier when managing users in an organization

As a bonus, a peek at HSBXL's implementation and a crash course in customizing the default schema to add your own custom attributes.
