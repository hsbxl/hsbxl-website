---
startdate: 2019-01-31
starttime: "17:30"
enddate: 2019-01-31
allday: false
linktitle: "Panpanpan: demonstration v2"
title: "Panpanpan: demonstration v2"
location: "HSBXL"
eventtype: "Demo"
price: "Free"
series: "byteweek2019"
image: "p-node.jpg"
---

a modular system of radio/streaming broadcast, composed of multiplers inputs and outputs. The box aims to provide a multi-functional and easy-to-use micro-fm streaming station. The PiBox has been developed within the collective ∏-node.org. The system is open source, and based on open source software / open hardware.
(half an hour)

https://p-node.org/documentation/pibox/piboxv2
https://wiki.hsbxl.be/PANPANPAN:_V-2_Install_Party

context:
a durational program to develop and implement a public performative interface for translocal radio telecommunication. While setting up securely an autonomous infrastructure and hardware, we empower ourselves by collective disembodiement, common voicing, and practices to commonize and decolonize our technologies.
http://ooooo.be/panpanpan/
