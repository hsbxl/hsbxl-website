---
title: "HSBXL mentions"
linktitle: "HSBXL mentions"
aliases: [/media]
---

An collection of 'HSBXL mentions' on media and other websites.

# 2023

- BRUZZ 2023-12-11 [https://www.bruzz.be/samenleving/samen-software-schrijven-hackerspace-bxl-maakt-oneigen-gebruik-van-technologie-2023-12](https://www.bruzz.be/samenleving/samen-software-schrijven-hackerspace-bxl-maakt-oneigen-gebruik-van-technologie-2023-12)

# 2018

- (archived) [https://www.bewustverbruiken.be/artikel/gedeelde-werkruimtes-een-overzicht](http://web.archive.org/web/20210128045924/https://www.bewustverbruiken.be/artikel/gedeelde-werkruimtes-een-overzicht)
- Video of the space we were occupying in 2018 [mp4](https://media.hsbxl.be/intro.mp4) [webm](https://media.hsbxl.be/intro.webm)

# 2017

- https://www.datapanik.org/events/software-freedom-day-hsbxl

# 2015

- (archived) [http://www.fablab-brussels.be/fablab/feestelijke-opening-hsbxl](http://web.archive.org/web/20160525034056/http://www.fablab-brussels.be/fablab/feestelijke-opening-hsbxl/)

# 2014

- Terzake 2014-12-11 - [on youtube](https://www.youtube.com/watch?v=7zMM5pNo1rc)
- http://www.makery.info/en/2014/11/04/une-semaine-dopen-lab-a-bruxelles

# 2013

- https://hackaday.com/2013/10/23/hackerspacing-in-europe-hsbxl-in-brussels
- http://fabelier.org/hello-world-from-hsbxl-hackerspace-in-brussels

# 2012

- [Brussel deze week - newspaper in Dutch](images/Brussel-deze-week-hsbxl-20120606.png)
  - or as PDF with HSBXL also on the front page! [HSBXL_in_BDW1332_on_2012-06-07.pdf](docs/HSBXL_in_BDW1332_on_2012-06-07.pdf)
  - https://www.bruzz.be/samenleving/bij-de-hackers-van-schaarbeek-geef-de-hardware-terug-aan-de-mensen-2012-06-08
- (archived) [www.aljazeera.net/news/reportsandinterviews/2012/5/24/هاكرز-ضد-الأفكار-النمطية](http://web.archive.org/web/20160302144919/http://www.aljazeera.net/news/reportsandinterviews/2012/5/24/%D9%87%D8%A7%D9%83%D8%B1%D8%B2-%D8%B6%D8%AF-%D8%A7%D9%84%D8%A3%D9%81%D9%83%D8%A7%D8%B1-%D8%A7%D9%84%D9%86%D9%85%D8%B7%D9%8A%D8%A9)
- Le Soir: https://www.lesoir.be/art/d-20120427-W09C7N (paywall)

# 2009

- Dorkbot BXL talk
  - last 19th of Feb we had a talk at dorkbot bxl (by ptr\_)
  - [http://farm4.static.flickr.com/3353/3305511683_f8673c2fc9_m.jpg](http://farm4.static.flickr.com/3353/3305511683_f8673c2fc9_m.jpg)
  - [http://farm4.static.flickr.com/3590/3306342318_635dea9dcc_m.jpg](http://farm4.static.flickr.com/3590/3306342318_635dea9dcc_m.jpg)
  - [http://farm4.static.flickr.com/3660/3306342384_7288a2f8d4_m.jpg](http://farm4.static.flickr.com/3660/3306342384_7288a2f8d4_m.jpg)
  - [http://farm4.static.flickr.com/3455/3306342426_28abf10d27_m.jpg](http://farm4.static.flickr.com/3455/3306342426_28abf10d27_m.jpg)
  - [http://farm4.static.flickr.com/3310/3306342486_52ae6fba9c_m.jpg](http://farm4.static.flickr.com/3310/3306342486_52ae6fba9c_m.jpg)
  - http://www.flickr.com/photos/malicy/3305511683
  - pics (c) by imal…
- ptr\_ is going over to Radio Panik tonight (Wed Feb 11, 2009) for a little talk on hackerspace bxl & our past openwrt hacktivities
  - [Good morning Stallman](https://www.radiopanik.org/emissions/good-morning-stallman/emission-du-11-fevrier/)

# 2008

- just before 25c3 hackerspaces.org sent out a quick rfi to all hackerspaces
  - and compiles those in a nicely layout e-book
  - a preliminary version of ours is [here](docs/038_voidpointer.pdf)

# hsbxl presentation

We used [this flyer](docs/Hsbxl-presentation.pdf) to present ourselves to  
the city of schaerbeek and get our fine 'Garage nr 48'
